{stdenv, cargo, rustc, libiconv, libpcap, pkg-config, zeromq}:
stdenv.mkDerivation rec {
  pname = "bifrost";
  version = "0.1.0";

  src = builtins.fetchGit {
    url = "https://gitlab.com/codemonkeylabs/bifrost";
    ref = "main";
  };

  buildInputs = [
    cargo
    rustc
    libiconv
    libpcap
    pkg-config
    zeromq
  ];
}
