extern crate cc;

fn main() {
    let mut c_sources = Vec::new();

    c_sources.push("c/src/l3.c");
    c_sources.push("c/src/l4.c");
    c_sources.push("c/src/pcap.c");
#[cfg(target_os = "linux")]
    c_sources.push("c/src/nfq.c");
    build(c_sources);
}

fn build(c_sources : Vec<&str>) {
    let mut builder = cc::Build::new();

    println!("cargo:rustc-link-lib=dylib=pcap");

    for source in c_sources.iter() {
        println!("cargo:rerun-if-changed={}", source);
        builder.file(source);
    }

#[cfg(target_os = "linux")]
    {
        println!("cargo:rustc-link-lib=dylib=netfilter_queue");
        println!("cargo:rustc-link-lib=dylib=nfnetlink");
        builder.flag("-DLINUX");
    }

#[cfg(debug_assertions)]
    builder.flag("-D__DEBUG__");
    builder.compile("libhiminbjoerg.a")
}
