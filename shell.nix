let
  base          = import <nixpkgs> {};
  pinnedVersion = base.pkgs.lib.importJSON ./nixpkgs.json;

  nixpkgs = base.pkgs.fetchFromGitHub {
    owner = "NixOS";
    repo  = "nixpkgs";
    inherit (pinnedVersion) rev sha256;
  };

  pkgs = import nixpkgs {};
in with pkgs; callPackage ./default.nix {}
