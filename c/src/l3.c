/*
 * Heimdall L3 specific handling
 *
 * Copyright (C) 2018 Erick Gonzalez
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#define _GNU_SOURCE

#include <errno.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <netinet/ip.h>
#include <netinet/ip6.h>
#include "huvud.h"
#include "l4.h"


#ifndef IPV6_VERSION_MASK
#define IPV6_VERSION_MASK	0xf0
#endif

#ifndef IPV6_VERSION
#define IPV6_VERSION		0x60
#endif

/* the AF_INET, AF_INET6 definitions are platform specific (!) .. ask me how I found out. So
 * use our own to guarantee that Haskell code sees always the same values
 */
#define H_AF_INET 2
#define H_AF_INET6 30

int parse_ip4(packet_t* pkt, struct ip* ip4, uint32_t len) {
  uint8_t  ip_hdr_len = ip4->ip_hl * 4;

  if (len < ip_hdr_len) {
    /* not enough data to even parse the IP header */
    goto err;
  }

  pkt->src_addr[0] = ip4->ip_src.s_addr;
  pkt->dst_addr[0] = ip4->ip_dst.s_addr;
  pkt->addr_len    = 1; /* length of the IP address in 32 bit words */
  pkt->family      = H_AF_INET;

  uint8_t* l4_data = (uint8_t*) (((uint32_t*)ip4) + ip4->ip_hl);
  uint32_t remains = len - ip_hdr_len;

  parse_l4(pkt, ip4->ip_p, l4_data, remains);

  return 1;
 err:
  return 0;
}

int parse_ip6(packet_t* pkt, struct ip6_hdr* ip6, uint32_t len) {

  if (len < sizeof(*ip6)) {
    /* not enough data to even parse the IP header */
    goto err;
  }

  memcpy(pkt->src_addr, &ip6->ip6_src, sizeof(pkt->src_addr));
  memcpy(pkt->dst_addr, &ip6->ip6_dst, sizeof(pkt->dst_addr));

  pkt->family   = H_AF_INET6;
  pkt->addr_len = 4; /* the IPv6 address is 4 x 32 bit words long */

  void*     l4_data = (void*) (((uint8_t*)ip6) + sizeof(*ip6));
  uint32_t  remains = len - sizeof(*ip6);

  parse_l4(pkt, ip6->ip6_nxt, l4_data, remains);

  return 1;
 err:
  return 0;
}

int parse_l3(uint32_t  id, uint8_t*  data, uint32_t  len, packet_t* pkt_p) {

  if (!pkt_p) {
    return -EINVAL;
  }

  // start assuming it is IP..

  struct ip* ip = (struct ip*)data;

  pkt_p->id        = id;
  pkt_p->pkt_len   = len;
  pkt_p->pkt_start = data;

  if (ip->ip_v == IPVERSION && ip->ip_hl >= 5) {
    if (!parse_ip4(pkt_p, ip, len)) {
#ifdef __DEBUG__
      char buf[4096];
      hexdump(buf, sizeof(buf), (uint8_t*)ip, min(ip->ip_hl * 4, len));
      fprintf(stderr, "Bad IPv4 Packet: %s", buf);
#endif
      return -EINVAL;
    }
  } else {
    struct ip6_hdr* ip6 = (struct ip6_hdr*)data;

    if ((ip6->ip6_vfc & IPV6_VERSION_MASK) == IPV6_VERSION) {
      if (!parse_ip6(pkt_p, ip6, len)) {
#ifdef __DEBUG__
        char buf[1024];
        hexdump(buf, sizeof(buf), ip, min(40, len));
        fprintf(stderr, "Bad IPv6 packet: %s", buf);
#endif
        return -EINVAL;
      }
    } else {
#ifdef __DEBUG__
      char buf[1024];
      hexdump(buf, sizeof(buf), data, min(64,len)); // limit preview to first 64 bytes
      fprintf(stderr, "Bypassing non IP packet: %s", buf);
#endif
      return ACCEPT_PACKET;
    }
  }

  return OK;
}

// verdict_t handle_l3_packet(huvud_queue_t* hq_p, uint32_t id, uint8_t* data, uint32_t len) {
//   packet_t pkt;

//   memset(&pkt, 0, sizeof(pkt));

//   int rc = parse_l3(id, data, len, &pkt);

//   if (rc == OK) {
// #ifdef __DEBUG__
//   char buf[4096];
//   hexdump(buf, sizeof(buf), data, min(128, len));
//   struct protoent* proto = getprotobynumber(pkt.protocol);
//   char src_ip[128];
//   char dst_ip[128];

//   inet_ntop(pkt.family, pkt.src_addr, src_ip, sizeof(src_ip));
//   inet_ntop(pkt.family, pkt.dst_addr, dst_ip, sizeof(dst_ip));

//   LOG(hq_p, DEBUG,
//         "Analyze %u bytes (of %u) @0x%p: %s %s(%u) %s:%u -> %s:%u : %s",
//         pkt.available_payload, pkt.total_payload, pkt.payload,
//         pkt.family == H_AF_INET ? "IPv4" : (pkt.family == H_AF_INET6 ? "IPv6" : "(unknown)"),
//         proto->p_name, pkt.protocol, src_ip, pkt.src_port, dst_ip, pkt.dst_port, buf);
// #endif
//     return analyze(hq_p, &pkt);
//   } else if (rc < 0) {
//     LOG(hq_p, WARNING, "L3 handling of packet failed: %s (%d)", strerror(rc), rc);
//   }
//   return ACCEPT_PACKET;
// }
