bifrost
=======
Smart user space network switch and firewall

### Getting started

#### Building

    cargo build [--release]

See under `targets/<debug|release>/`:
- bifrost : _the user space application itself_
- huvud: _tool for manual control_

#### Usage

- `bifrost --help` is your friend

#### Controlling the bifrost

Heimdall controls the bifrost. So in principle after starting it, all configuration falls upon Heimdall. You can however issue manual commands with the huvud tool. `huvud --help` should get you started.
