use byteorder::{ByteOrder, NetworkEndian};
use core::fmt::{Display, Formatter};
use crate::Error;
use std::mem::ManuallyDrop;
use std::ops::Deref;

pub type IPAddr = std::net::IpAddr;

#[repr(C)]
pub struct CIPv4Addr([u8; 4]);
#[repr(C)]
pub struct CIPv6Addr([u8; 16]);

impl From<&CIPv4Addr> for IPAddr {
    fn from(CIPv4Addr(bytes): &CIPv4Addr) -> Self {
        IPAddr::V4(std::net::Ipv4Addr::from(NetworkEndian::read_u32(bytes)))
    }
}

impl From<&CIPv6Addr> for IPAddr {
    fn from(CIPv6Addr(bytes):   &CIPv6Addr) -> Self {
        IPAddr::V6(std::net::Ipv6Addr::from(NetworkEndian::read_u128(bytes)))
    }
}

#[repr(C)]
pub union CIPAddr {
    pub v4:     ManuallyDrop<CIPv4Addr>
  , pub v6:     ManuallyDrop<CIPv6Addr>
  , pub bytes:  [u32; 4]
}

#[repr(C)] #[derive(Debug)]
pub struct CProtocol(u8);

#[repr(C)]
pub struct Packet {
    id:                u32
  , src_bytes:         CIPAddr
  , dst_bytes:         CIPAddr
  , addr_len:          u8
  , family:            u8
  , protocol:          CProtocol
  , unused:            u8
  , src_port:          u16
  , dst_port:          u16
  , total_payload:     u32
  , available_payload: u32
  , pkt_len:           u32
  , pkt_start:         *const u8
  , payload:           *const u8
}

impl Packet {
    pub fn src_ip(&self) -> Result<IPAddr, Error>  {
        self.into_ip(&self.src_bytes)
    }
    pub fn dst_ip(&self) -> Result<IPAddr, Error>  {
        self.into_ip(&self.dst_bytes)
    }
    pub fn into_ip(&self, c_ip: &CIPAddr) -> Result<IPAddr, Error> {
        unsafe {
            match self.addr_len {
                1 => Ok(c_ip.v4.deref().into())
              , 4 => Ok(c_ip.v6.deref().into())
              , x => Err(Error::InvalidData(format!("Invalid address length {}", x)))
            }
        }
    }
}

impl Display for Packet {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), std::fmt::Error> {
        write!(f, "#{} {:?} {} > {}", self.id, self.protocol, self.src_ip()?, self.dst_ip()?)
    }
}