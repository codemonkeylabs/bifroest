use crate::*;
use crate::interface::{FutureService};
use crate::packet::Packet;
use crate::shared_ptr::Shared;
use crate::switch::RxContext;
use crate::switch::Switch;
use libc::c_void;
use std::ffi::{CStr};
use std::ptr;
use std::sync::Arc;
use tokio::io::unix::AsyncFd;

pub struct Platform {
    pub session: Arc<CBox<*mut pcap_t>>
  , pub fp:      Arc<CBox<*mut bpf_program_t>>
  , pub fd:      i32
}

opaque_c_struct!(pcap_if_t);
opaque_c_struct!(pcap_t);
opaque_c_struct!(pcap_pkthdr_t);
opaque_c_struct!(bpf_program_t);

// we are promising that pcap_t and bpf_program_t can be moved accross threads
unsafe impl Send for CBox<*mut pcap_t> {}
unsafe impl Send for CBox<*mut bpf_program_t> {}

// .. and that they can be used simultaneously by different threads.. 👀
unsafe impl Sync for CBox<*mut pcap_t> {}
unsafe impl Sync for CBox<*mut bpf_program_t> {}

const MAX_CAPTURE_SIZE : i32 = 8192;

#[allow(dead_code)]
extern "C" {
    static C_PCAP_ERRBUF_SIZE: i32;
    fn pcap_findalldevs(alldevsp : *mut *const pcap_if_t, errbuf: *mut i8) -> i32;
    fn pcap_freealldevs(alldevsp : *const pcap_if_t);
    fn pcap_nextdev(dev: *const pcap_if_t) -> *const pcap_if_t;
    fn pcap_devname(dev: *const pcap_if_t) -> *const i8;
    fn pcap_open_live(dev: *const i8, snaplen : i32, promisc: i32, to_ms: i32, errbuf: *mut i8) -> *mut pcap_t;
    fn pcap_close (sesh: *mut pcap_t);
    fn pcap_datalink(sesh: *const pcap_t) -> i32;
    fn datalink_name(link_type: i32) -> *const i8;
    fn pcap_alloc_fp() -> *mut bpf_program_t;
    fn pcap_free_fp(fp: *mut bpf_program_t);
    fn pcap_compile(sesh: *const pcap_t, fp: *mut bpf_program_t, exp: *const i8, opt: i32, mask: u32) -> i32;
    fn pcap_geterr(sesh: *const pcap_t) -> *const i8;
    fn pcap_lookupnet(dev: *const i8, addr: *const u32, mask: *const u32, errbuf: *mut i8) -> i32;
    fn pcap_setfilter(sesh: *mut pcap_t, fp: *const bpf_program_t) -> i32;
    fn pcap_get_selectable_fd(sesh: *const pcap_t) -> i32;
    fn pcap_dispatch(sesh: *const pcap_t, count: i32, callback: unsafe extern "C" fn(ctx: *mut u8, hdr: *const pcap_pkthdr_t, frame: *const u8), ctx: *mut c_void) -> i32;
}

pub fn init (id: &interface::Id, device: Option<String>, filter_exp: Option<String>) -> Result<Box<Platform>, Error> {
    let mut device = device;
    unsafe {
        let mut errbuf = vec![0; C_PCAP_ERRBUF_SIZE.try_into().unwrap()];
        if let None = device {
            let mut alldevsp : *const pcap_if_t = std::ptr::null();
            let rc = pcap_findalldevs(&mut alldevsp, errbuf.as_mut_ptr());
            let alldevsp = CBox::new(alldevsp, Box::new(|p| pcap_freealldevs(*p)));
            if  rc != 0 {
                return Err(Error::IOError(from_c_string!(errbuf.as_ptr())))
            }
            if *alldevsp != ptr::null() {
                device = Some(CStr::from_ptr(pcap_devname(*alldevsp)).to_str()?.to_string());
            } else {
                return Err(Error::InvalidArgument("No device for capture".to_string()))
            }
        }
        let c_device = c_string!(device.as_ref().unwrap().clone());
        let session = pcap_open_live(c_device.as_ptr(), MAX_CAPTURE_SIZE, 0, 100 /* ms */, errbuf.as_mut_ptr());
        if &*session as *const pcap_t== ptr::null() {
            let msg = format!("Failed to start pcap: {}", from_c_string!(errbuf.as_ptr()));
            return Err(Error::IOError(msg));
        }
        let session   = CBox::new(session, Box::new(|sesh| pcap_close(*sesh)));
        let link_type = pcap_datalink(*session);
        let link_name = CStr::from_ptr(datalink_name(link_type)).to_str()?;
        let fp        = pcap_alloc_fp();
        let c_exp     = c_string!(filter_exp.unwrap_or("".to_string()));
        let addr      = 0;
        let mask      = 0;
        if pcap_lookupnet(c_device.as_ptr(), &addr, &mask, errbuf.as_mut_ptr()) != 0 {
            return Err(Error::IOError(from_c_string!(errbuf.as_ptr())))
        }

        if pcap_compile(*session, fp, c_exp.as_ptr(), 1, mask) != 0 {
            let err = CStr::from_ptr(pcap_geterr(*session)).to_str()?;
            return Err(Error::InvalidArgument(format!("Failed to interpret BP filter expression: {}", err)));
        };
        let fp = CBox::new(fp, Box::new(|ptr| pcap_free_fp(*ptr)));

        if pcap_setfilter(*session, *fp) != 0 {
            let err = CStr::from_ptr(pcap_geterr(*session)).to_str()?;
            return Err(Error::IOError(format!("Failed to asssigh BP filter to PCAP session: {}", err)));
        }
        let fd = pcap_get_selectable_fd(*session);
        if fd == -1 {
            log::warn!(target: "pcap", "no selectable fd for capture on {}", device.as_ref().unwrap());
        }
        log::info!(target: "pcap", "Opened {} (type {}) device {} for {}", link_name, link_type, device.unwrap(), id);
        Ok(Box::new(Platform{session: Arc::new(session), fp: Arc::new(fp), fd}))
    }
}

extern "C" fn handle_frame(_ctx: *mut u8, _hdr: *const pcap_pkthdr_t, _frame: *const u8) {

}

impl Platform {
    pub fn service(&self, intf: interface::Ptr, switch: Shared<Switch>, _rx_packet: unsafe extern "C" fn(ctx: *mut c_void, Packet) ) -> FutureService {
        let fd      = self.fd;
        let session = self.session.clone();

        Box::pin(async move { unsafe {
            let read_op = AsyncFd::new(fd)?;
            loop {
                let mut ctx = RxContext{switch: &switch, input_intf: &intf};
                read_op.readable().await?
                       .try_io(|_| Ok(pcap_dispatch(**session, 1, handle_frame, &mut ctx as *mut RxContext as *mut c_void)))
                       .unwrap_or(Ok(0))?;
            }
        } } )
    }
}
