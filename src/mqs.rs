use async_zmq::Multipart;
use crate::Error;

pub mod butler;

pub struct Response<T> {
    pub reply:  Option<Multipart>,
    pub result: Result<T, Error>
}
