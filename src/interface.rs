pub mod queue;

use core::future::Future;
use core::pin::Pin;
use crate::packet::Packet;
use crate::shared_ptr::Shared;
use crate::switch::Switch;
use crate::{Error, show};
use libc::c_void;
use serde::{Serialize, Deserialize};
use std::str::FromStr;

#[derive(Clone, PartialEq, Eq, Hash, Serialize, Deserialize)]
pub enum Kind {
    GRE
  , Queue
  , VIF
}

show!{Kind,
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Kind::GRE   => write!(f, "gre")
          , Kind::Queue => write!(f, "queue")
          , Kind::VIF   => write!(f, "vif")
        }
    }
}

impl FromStr for Kind {
    type Err = Error;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "gre"   => Ok(Kind::GRE),
            "queue" => Ok(Kind::Queue),
            "vif"   => Ok(Kind::VIF),
            _       => Err(Error::InvalidData(format!("Interface kind: {}", s)))
        }
    }
}
#[derive(Clone, PartialEq, Eq, Hash, Serialize, Deserialize)]
pub struct Id(Kind, u32, u32);

impl Id {
    pub fn kind(&self) -> &Kind {
        &self.0
    }
}

show!{Id,
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}/{}/{}", self.0, self.1, self.2)
    }
}

impl FromStr for Id {
    type Err = Error;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let parts : Vec<&str> = s.split('/').collect();
        if parts.len() != 3 {
            return Err(Error::InvalidData("Interface Id must be of the form <kind>/<sub>/<num>".to_string()))
        }

        let kind = parts[0].parse()?;
        let sub  = parts[1].parse()?;
        let num  = parts[2].parse()?;
        Ok(Id(kind, sub, num))
    }
}

type FutureService = Pin<Box<dyn Future<Output = Result<(), Error>> + Send>>;

pub trait Interface {
    fn id(&self) -> &Id;
    fn service(&self, switch: Shared<Switch>, rx_packet: unsafe extern "C" fn (*mut c_void, Packet)) -> FutureService;
}

pub type Dyn = dyn Interface + Sync + Send;
pub type Ptr = Shared<Dyn>;