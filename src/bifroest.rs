use bifroest::{Env, Error};
use bifroest::logging;
use bifroest::controller;
use bifroest::switch;
use clap;
use tokio;

#[tokio::main]
async fn main() -> Result <(), Error> {
	let yaml = clap::load_yaml!("bifroest.yaml");
    let args = clap::App::from_yaml(yaml).get_matches();

    let mut env    = Env{switch: switch::Switch::spawn()
                       , logger: logging::init(args.value_of("log"))};

    log::warn!(target: "application", "Opening the bifröst");

    let result = tokio::select!{
    	res = controller::drive(args.value_of("controller").unwrap(), &mut env) => {
    		log::warn!(target: "controller", "terminated (shutdown?)");
    		res
    	}
    };
  	log::warn!(target: "application", "Bifröst is down");
    result
}
