use crate::{Error, Env};
use crate::mqs::*;
use ciborium::de::from_reader;
use ciborium::ser::into_writer;
use futures::future;
use futures::{TryFutureExt};
use crate::interface;
use log;
use crate::logging::{LevelFilter};
use serde::{Serialize, Deserialize};
use std::ops::{Deref};
use tokio::time::{sleep, Duration};

#[derive(Eq, PartialEq, Serialize, Deserialize, Debug)]
pub enum Message {
    Ack
  , AttachInterface { id:         interface::Id
                    , device:     Option<String>
                    , filter_exp: Option<String>
                    }
  , BadRequest { reason: String }
  , DettachInterface { id: interface::Id }
  , FailedRequest { failure: String }
  , NotSupported
  , RemoveLogTargets { modules: Vec<String> }
  , SetLogTargets { targets: Vec<(Option<String>, LevelFilter)> }
  , Shutdown
}

pub const MAX_ENCODED_MESSAGE_SIZE: usize = 8192;

pub async fn drive (endpoint: &str, env: &mut Env) -> Result<(), Error> {
    loop {
        butler::run(endpoint, |parts| {
            // only a single frame is expected.. if there are more, just
            // ignore the others
            match parts.get(0) {
                Some(frame) => {
                    log::trace!(target: "controller", "Received ØMQ frame: {:?}", frame);
                    match decode(&frame) {
                        Ok(msg)  =>
                            futures::executor::block_on(handle(msg, env)),
                        Err(err) => {
                            log::info!(target: "controller", "Message deserialization failed: {}", err);
                            respond(Message::BadRequest{ reason: format!("Malformed request: {}", err) }, Ok(()))
                        }
                    }
                },
                None => {
                    log::info!(target: "controller", "Unexpected empty message received ¯\\_(ツ)_/¯");
                    respond(Message::BadRequest{ reason: "Empty request".to_string() }, Ok(()))
                }
            }
        }).or_else(|err| {
            match err {
                Error::Aborted(_) => {
                    log::debug!(target:"controller", "Service terminated: {}", err);
                    future::err(err)
                },
                _ => {
                    // These are recoverable errors. Yield Ok() to run the service once more
                    log::error!(target:"controller", "Service interruption: {}", err);
                    future::ok(())
                }
            }
        }).await?;
        sleep(Duration::from_millis(2378)).await
    }
}

pub fn decode(frame: &zmq::Message) -> Result <Message, Error> {
    Ok(from_reader(frame.deref())?)
}

pub fn encode(msg: Message) -> Result <zmq::Message, Error> {
    let mut buffer:Vec<u8> = vec![];
    Ok(into_writer(&msg, &mut buffer).map(|()| async_zmq::Message::from(buffer))?)
}

pub fn respond<T>(msg: Message, result: Result<T, Error>) -> Response<T> {
    match encode(msg) {
        Ok(frame) => Response{reply: Some(vec![frame]), result},
        Err(err)  => Response{reply: None, result: Err(err)}
    }
}

macro_rules! failed_request {
    ($err:expr) => {
        respond(Message::FailedRequest{failure: format!("{}", $err)}, Err($err))
    };
}

macro_rules! respond_ok {
    ($val:expr) => {
        respond(Message::Ack, Ok($val))
    };
}

async fn handle(msg: Message, env: &mut Env) -> Response<()> {
    match msg {
        Message::AttachInterface{id, device, filter_exp} => {
            env.switch.attach_interface(&id, device, filter_exp).await.map_or_else(
                |err| failed_request!(err),
                |res| respond_ok!(res)
            )
        },
        Message::SetLogTargets{targets} => {
            for (module, level) in targets {
                env.logger.set_target(module, level);
            }
            respond_ok!(())
        },
        Message::RemoveLogTargets{modules} => {
            for module in modules {
                env.logger.remove_target(module);
            }
            respond_ok!(())
        },
        Message::Shutdown => {
            respond(Message::Ack, Err(Error::Aborted("Received shutdown order".to_string())))
        },
        _ => {
            respond(Message::NotSupported, Err(Error::Unimplemented("No handling of {}".to_string())))
        }
    }
}
